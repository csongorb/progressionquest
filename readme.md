# Progression Quest
- a.k.a. *Grow Your Little Game Designer*
- a.k.a. *My Little Game Designer*

A **customizable teaching / learning system / structure** for (not only university) courses / workshops.

- optional learning objectives
- (still) BA / MA compatible grading system

A **set of resources & examples** for lecturers / professors / workshop instructors.

Teaching Game Design / Interactive Media / Digital Media / Interactive Narratives is an **ongoing & ever-evolving experiment**, with still too many open questions. *Progression Quest* is in a way connected to...

- my previous experiences in...
	- teaching
	- structuring long-term learning environments
- [PornCreator](https://bitbucket.org/csongorb/porncreator/)
	- [Dissecting Games](http://dissectinggames.csongorb.com)
	- [Narrative Design Porn](http://narrativedesignporn.csongorb.com)
	- [Level Design Porn](http://leveldesignporn.csongorb.com)
- [The Fun of It - A personal heap of fragments and quotes about teaching and game design](https://medium.com/@csongorb/the-fun-of-it-b80852a19834)

## Structure

- [`01_progressionSystem /`](/01_progressionSystem/)` Progression System Overview`
	- the general rules of the progression system
- [`02_questBoards /`](/02_questBoards/)` Quest Board Examples`
	- structure of the course / workshop submissions
	- some new & old examples
- [`03_templates /`](/03_templates/)
	- `Quest Book Template`
		- more detailed description of the quests
		- designed for printing
	- `Schedule Template`
		- overview of the course / workshop schedule
		- management of the limited quests
	- `Character Sheet Template`
		- visible tracking of the individual progression of students / participants

## How To...?

- take a look at the documents
- adjust them accordingly to your needs
	- match them with your...
		- *topics / further readings*
		- *tasks / excercises*
		- *quest selection methodology*
		- *schedule / rhythm*
		- *tools*
		- *etc.*
- handle out analogue versions of the Quest Book
	- incl. some stickers!
- or just take the whole package as inspiration

## Log / Version-History

### Todos / Bugs / Ideas

- noted, ideas and feedback from students / participants (v.5)
	- ?
- notes and feedback from students / participants (v0.4)
	- Determination Coin system is generally working
		- keeping track of the Coins through the Character Sheet is enough though
	- thumbs metaphor works well
- notes and feedback from students / participants (v0.3)
	- add balancing tips for quest design
	- use more standartized quest descriptions
		- more inspired by a normal syllabus
			- see [Douglas Wilson: Game-a-Week](https://docs.google.com/document/d/1JNLojNIQNwn80043PUJ7ZyYXOjtAI4XHJibvtpe7El4/edit)
		- more focused on the qualification objectives
			- what should a student / participant know / understand / be able to do after the quest?
	- update Character Sheets
		- make them more individual, what kind of a person are you?
- notes and feedback from students / participants (v0.1 & v0.2)
	- optional objectives not used
		- too hard to track during the course / semester
		- how to integrate them better?
	- too much freedom, no guidance (?)
	- quantity counts more than quality (?)

### Release 0.6 (Work in Progress)

### Release 0.5 (October 2019, though commits were finished 6 months later)

Fixes / improvements:

- updated `Progression System Overview`
	- system adjusted to a fixed amount of Reputation Impact Points
	- got lost of the Busy Bee Points
	- thus: quality counts more than quantity
	- got lost of Course Quests
	- added Side and Additional Quests
- improved `Character Sheet` template
	- improved according to the changes further above
- new & updated example course (`P1 / Mechanics`)
	- got lost of the cut-able Determination Coins
	- improved Sticker Album aesthetics & metaphor

### Release 0.4 (March 2019, though commits were finished 6 months later)

Fixes / improvements:

- improved `Character Sheet` template
	- added Determination Coins
	- removed Level (no need for this, just made things too complicated)
	- added color coding for different feedback stages
	- added row for "a personal message"
	- other small adjustments
	- better visual feedback for individual progress
- added `Quest Book` template
	- idea
		- you can make an analogue book for every student / participant now
		- incl. stickers!
	- added general rules
	- added placeholders for Progression System and Quest Board
- updated `Progression System Overview`
	- page size changed to A5
	- metaphor of *thumbs* added (complementary to reputation)
		- was it good? thumbs up!
		- was it bad? thumbs down!
		- was it ok? thumbs middle...
	- small adjustment for better communication
	- some general rules removed for later relocation
	- added visualisation possibility for...
		- several Main Quests, that the students / participants can choose from
- added Determination Coin system
	- for quest bidding

### Release 0.3 (September 2018)

Fixes / improvements:

- improved template for `Character Sheets`
	- added student / participant overview
	- improved progression visualisation
	- removed achievements
- improved `Progression System Overview`
	- added determination points
		- (much) better defined quest selection methodology (for limited quests)
	- improved division between raid and course quests
		- as replacement for achievements
	- added general rules
		- to avoid repetitions on every quest board
- added template for `Schedule`
	- overview of all course / workshop dates and deadlines
	- management of limited quests (incl. determination points)
- added template for `Quest Books`
	- more detailed description of the quests
- updated examples for `Quest Boards`
	- added new examples (incl. PDFs)
		- `Game Gardening Simulator 2018`
		- `Layers in Game Design: Level Design & Balancing`
		- `Project 1 / Mechanics`
	- removed old ones (kept old PDFs)
- improved file structure

### Release 0.2 (March 2018)

Metaphor changed from *achievements* to **quests**, incl. all / some cool consequences.

Fixes / improvements:

- new structure
	- `Progression System Overview`
		- as [OnePage](http://www.stonetronix.com/gdc-2010/) [Design](https://www.gdcvault.com/play/1012356/One-Page)
		- explaining the system / the rules
	- `Character Sheets`
		- one sheet per student / participant
		- more visible individual progression
	- `Quest Boards`
		- example courses / workshops
			- `Build the Toy First`
			- `On Storyworlds - Dissecting Star Wars`
			- `These boots are made for walking...`
		- better overviews
	- no general Course Description documents anymore
		- all infos bundled in the other overviews
- additional quest related improvements
	- main quests, side quests
	- raids, limited amount quests
	- etc.
- and a lot more!

### Release 0.1 (September 2017)

- first release
	- highly building on previous courses / teaching experiences of the last years
- main features
	- achievements for extending the base requirements of a course
	- two templates (in german)
		- `Course Description`, incl. a structure for...
			- ...base requirements
			- ...additional achievements
		- `Achievement Sheet`
			- tracking the progress of all students on one sheet
			- feedback-cell for each achievement
			- good overview
	- two achievement structure examples (in german)
		- `Game Design as Gardening`
			- course based on the examination of the development process of a game
		- `Prototype or Die`
			- project development module
			- additional achievements for additional tasks dealing with the topic of death (in games)

## Credits

- **csongor baranyai**  
	- csongorb (at) gmail (dot) com  
	- [www.csongorb.com](http://www.csongorb.com)

Thanks to all **universities / schools / institutions** ([MD.H](http://www.mediadesign.de/), [ifs](http://www.filmschule.de), [Universität Bayreuth](https://computerspielwissenschaften.uni-bayreuth.de/) and [Game Design // UE](http://gamedesign.ue-germany.de), etc.) where I had the possibility to give lectures / courses / workshops and refine the ideas of this system / structure.

Thanks for [Sebastian Stamm](http://www.the-stamm.com) heavily contributing to this project by joining me on a course, which we have designed with the help of this system, with all his input and feedback and improvement-ideas.

And of course a huge thank you to all the **students / participants** who contributed through all their comments and critiques... and tolerated my experiments.

Inspired by:

- [Institute of Play](https://www.instituteofplay.org/gll-principles)
- [Extra Credits: Gamifying Education](https://www.youtube.com/watch?v=MuDLw1zIc94)
- [Ian Schreiber: A Course About Game Balance (GDC16)](https://www.gdcvault.com/play/1023349/A-Course-About-Game)
- ...and many more!

Made with:

- [OmniGraffle Pro](https://en.wikipedia.org/wiki/OmniGraffle), Version 7.9.4
- [Microsoft Word for Mac](https://en.wikipedia.org/wiki/Microsoft_Word), Version 15.33
- [Microsoft Excel for Mac](https://en.wikipedia.org/wiki/Microsoft_Excel), Version 15.33

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).

All other media (images, software, etc.) remain the property of their copyright holders.
